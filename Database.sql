USE [master]
GO
/****** Object:  Database [MovieCharacterDB]    Script Date: 9/8/2020 3:20:40 PM ******/
CREATE DATABASE [MovieCharacterDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MovieCharacterDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\MovieCharacterDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MovieCharacterDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\MovieCharacterDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [MovieCharacterDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MovieCharacterDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MovieCharacterDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [MovieCharacterDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MovieCharacterDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MovieCharacterDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [MovieCharacterDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MovieCharacterDB] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [MovieCharacterDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MovieCharacterDB] SET  MULTI_USER 
GO
ALTER DATABASE [MovieCharacterDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MovieCharacterDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MovieCharacterDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MovieCharacterDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MovieCharacterDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MovieCharacterDB] SET QUERY_STORE = OFF
GO
USE [MovieCharacterDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 9/8/2020 3:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Actors]    Script Date: 9/8/2020 3:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[MiddleName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Gender] [int] NOT NULL,
	[DOB] [datetime2](7) NOT NULL,
	[PlaceOfBirth] [nvarchar](max) NULL,
	[Biography] [nvarchar](max) NULL,
	[Picture] [nvarchar](max) NULL,
 CONSTRAINT [PK_Actors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Characters]    Script Date: 9/8/2020 3:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Characters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[Gender] [int] NOT NULL,
	[Picture] [nvarchar](max) NULL,
 CONSTRAINT [PK_Characters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Franchises]    Script Date: 9/8/2020 3:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Franchises](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Franchises] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MovieCharacters]    Script Date: 9/8/2020 3:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovieCharacters](
	[MovieId] [int] NOT NULL,
	[CharacterId] [int] NOT NULL,
	[ActorId] [int] NOT NULL,
	[Picture] [nvarchar](max) NULL,
 CONSTRAINT [PK_MovieCharacters] PRIMARY KEY CLUSTERED 
(
	[MovieId] ASC,
	[CharacterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movies]    Script Date: 9/8/2020 3:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Genre] [nvarchar](max) NULL,
	[ReleaseYear] [datetime2](7) NOT NULL,
	[Director] [nvarchar](max) NULL,
	[Picture] [nvarchar](max) NULL,
	[Trailer] [nvarchar](max) NULL,
	[FranchiseId] [int] NULL,
 CONSTRAINT [PK_Movies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200830170158_MovieDB', N'3.1.7')
GO
SET IDENTITY_INSERT [dbo].[Actors] ON 

INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (1, N'Robert', N'John', N'Downey Jr.', 0, CAST(N'1965-04-04T00:00:00.0000000' AS DateTime2), N'New York City, New York, USA', N'American actor, producer, and singer. His career has been characterized by critical and popular success in his youth, followed by a period of substance abuse and legal troubles, before a resurgence of commercial success in middle age.', N'https://m.media-amazon.com/images/M/MV5BNzg1MTUyNDYxOF5BMl5BanBnXkFtZTgwNTQ4MTE2MjE@._V1_UX214_CR0,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (2, N'Chris', N'Robert', N'Evans', 0, CAST(N'1981-06-13T00:00:00.0000000' AS DateTime2), N'Boston, Massachusetts, USA', N'Bio2', N'https://m.media-amazon.com/images/M/MV5BMTU2NTg1OTQzMF5BMl5BanBnXkFtZTcwNjIyMjkyMg@@._V1_UY317_CR6,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (3, N'Mark', NULL, N'Ruffalo', 0, CAST(N'1967-11-22T00:00:00.0000000' AS DateTime2), N'Kenosha, Wisconsin, USA', N'Bio3', N'https://m.media-amazon.com/images/M/MV5BNDQyNzMzZTMtYjlkNS00YzFhLWFhMTctY2M4YmQ1NmRhODBkXkEyXkFqcGdeQXVyNjcyNzgyOTE@._V1_UY317_CR20,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (4, N'Chris', NULL, N'Hemsworth', 0, CAST(N'1983-08-11T00:00:00.0000000' AS DateTime2), N'Melbourne, Victoria, Australia', N'Bio4', N'https://m.media-amazon.com/images/M/MV5BOTU2MTI0NTIyNV5BMl5BanBnXkFtZTcwMTA4Nzc3OA@@._V1_UX214_CR0,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (5, N'Scarlett', NULL, N'Johansson', 1, CAST(N'1984-11-22T00:00:00.0000000' AS DateTime2), N'New York City, New York, USA', N'Bio5', N'https://m.media-amazon.com/images/M/MV5BMTM3OTUwMDYwNl5BMl5BanBnXkFtZTcwNTUyNzc3Nw@@._V1_UY317_CR23,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (6, N'Jeremy', N'Lee', N'Renner', 0, CAST(N'1971-01-07T00:00:00.0000000' AS DateTime2), N'Modesto, California, USA', N'Bio6', N'https://m.media-amazon.com/images/M/MV5BOTk2NDc2ODgzMF5BMl5BanBnXkFtZTcwMTMzOTQ4Nw@@._V1_UX214_CR0,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (7, N'Don', N'Frank', N'Cheadle', 0, CAST(N'1964-11-29T00:00:00.0000000' AS DateTime2), N'Kansas City, Missouri, USA', N'Bio7', N'https://m.media-amazon.com/images/M/MV5BNDMxNDM3MzY5N15BMl5BanBnXkFtZTcwMjkzOTY4MQ@@._V1_UY317_CR17,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (8, N'Paul', N'Stephen', N'Rudd', 0, CAST(N'1969-04-06T00:00:00.0000000' AS DateTime2), N'Passaic, New Jersey, USA', N'Bio8', N'https://m.media-amazon.com/images/M/MV5BMTY4NTEwNDg1MV5BMl5BanBnXkFtZTgwODMwMDA0ODE@._V1_UY317_CR20,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (9, N'Benedict', N'Timophy', N'Cumberbatch', 0, CAST(N'1976-07-19T00:00:00.0000000' AS DateTime2), N'Passaic, New Jersey, USA', N'Bio9', N'https://m.media-amazon.com/images/M/MV5BMjE0MDkzMDQwOF5BMl5BanBnXkFtZTgwOTE1Mjg1MzE@._V1_UY317_CR2,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (10, N'Chadwick', NULL, N'Boseman', 0, CAST(N'1976-11-29T00:00:00.0000000' AS DateTime2), N'Passaic, New Jersey, USA', N'Bio10', N'https://m.media-amazon.com/images/M/MV5BMTk2OTY5MzcwMV5BMl5BanBnXkFtZTgwODM4MDI5MjI@._V1_UX214_CR0,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (11, N'Brie', NULL, N'Larson', 1, CAST(N'1989-10-01T00:00:00.0000000' AS DateTime2), N'Sacramento, California, USA', N'Bio11', N'https://m.media-amazon.com/images/M/MV5BMjExODkxODU3NF5BMl5BanBnXkFtZTgwNTM0MTk3NjE@._V1_UY317_CR7,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (12, N'Tom', N'Stanley', N'Holland', 0, CAST(N'1996-06-01T00:00:00.0000000' AS DateTime2), N'Kingston upon Thames, England, UK', N'Bio12', N'https://m.media-amazon.com/images/M/MV5BNTAzMzA3NjQwOF5BMl5BanBnXkFtZTgwMDUzODQ5MTI@._V1_UY317_CR23,0,214,317_AL_.jpg')
INSERT [dbo].[Actors] ([Id], [FirstName], [MiddleName], [LastName], [Gender], [DOB], [PlaceOfBirth], [Biography], [Picture]) VALUES (13, N'Josh', NULL, N'Brolin', 0, CAST(N'1968-02-12T00:00:00.0000000' AS DateTime2), N'Kingston upon Thames, England, UK', N'Bio12', N'https://m.media-amazon.com/images/M/MV5BNTAzMzA3NjQwOF5BMl5BanBnXkFtZTgwMDUzODQ5MTI@._V1_UY317_CR23,0,214,317_AL_.jpg')
SET IDENTITY_INSERT [dbo].[Actors] OFF
GO
SET IDENTITY_INSERT [dbo].[Characters] ON 

INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (1, N'Tony Stark', N'Iron Man', 0, N'https://i.pinimg.com/736x/37/7f/e3/377fe3095549bed0cef8b50d3bbd27cd.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (2, N'Steve Rogers', N'Captain America', 0, N'https://vignette.wikia.nocookie.net/earth-199999/images/7/72/CapAmerica.jpg/revision/latest/top-crop/width/360/height/450?cb=20180816180257')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (3, N'Steve Bannon', N'Incredible Hulk', 0, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (4, N'Thor Odinson', N'Thor', 0, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (5, N'Natasha Romanoff', N'Black Widow', 1, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (6, N'Clint Barton', N'Hawkeye', 0, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (7, N'James Rhodes', N'War Machine', 0, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (8, N'Scott Lang', N'Ant-Man', 0, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (9, N'Stephen Strange', N'Doctor Strange', 0, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (10, N'King T''Challa', N'Black Panther', 0, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (11, N'Carol Danvers', N'Captain Marvel', 1, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (12, N'Peter Parker', N'Spider-man', 0, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
INSERT [dbo].[Characters] ([Id], [FullName], [Alias], [Gender], [Picture]) VALUES (13, N'Thanos', N'Thanos', 0, N'https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg')
SET IDENTITY_INSERT [dbo].[Characters] OFF
GO
SET IDENTITY_INSERT [dbo].[Franchises] ON 

INSERT [dbo].[Franchises] ([Id], [Name], [Description]) VALUES (1, N'Marvel Cinematic Universe', N'Better universe than DC tbh')
SET IDENTITY_INSERT [dbo].[Franchises] OFF
GO
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 1, 1, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 2, 2, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 3, 3, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 4, 4, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 5, 5, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 6, 6, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 7, 7, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 8, 8, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 9, 9, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 10, 10, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 11, 11, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 12, 12, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (1, 13, 13, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 1, 1, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 2, 2, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 3, 3, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 4, 4, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 5, 5, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 6, 6, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 7, 7, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 8, 8, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 9, 9, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 10, 10, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 11, 11, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 12, 12, NULL)
INSERT [dbo].[MovieCharacters] ([MovieId], [CharacterId], [ActorId], [Picture]) VALUES (2, 13, 13, NULL)
GO
SET IDENTITY_INSERT [dbo].[Movies] ON 

INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (1, N'Avengers: Infinity War', N'Action,Adventure,Sci-Fi', CAST(N'2018-04-27T00:00:00.0000000' AS DateTime2), N'Anthony Russo, Joe Russo', N'https://m.media-amazon.com/images/M/MV5BMjMxNjY2MDU1OV5BMl5BanBnXkFtZTgwNzY1MTUwNTM@._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.youtube.com/watch?v=6ZfuNTqbHE8', 1)
INSERT [dbo].[Movies] ([Id], [Title], [Genre], [ReleaseYear], [Director], [Picture], [Trailer], [FranchiseId]) VALUES (2, N'Avengers: Endgame', N'Action,Adventure,Drama', CAST(N'2019-04-26T00:00:00.0000000' AS DateTime2), N'Anthony Russo, Joe Russo', N'https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_UX182_CR0,0,182,268_AL_.jpg', N'https://www.youtube.com/watch?v=TcMBFSGVi1c', 1)
SET IDENTITY_INSERT [dbo].[Movies] OFF
GO
/****** Object:  Index [IX_MovieCharacters_ActorId]    Script Date: 9/8/2020 3:20:40 PM ******/
CREATE NONCLUSTERED INDEX [IX_MovieCharacters_ActorId] ON [dbo].[MovieCharacters]
(
	[ActorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_MovieCharacters_CharacterId]    Script Date: 9/8/2020 3:20:40 PM ******/
CREATE NONCLUSTERED INDEX [IX_MovieCharacters_CharacterId] ON [dbo].[MovieCharacters]
(
	[CharacterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Movies_FranchiseId]    Script Date: 9/8/2020 3:20:40 PM ******/
CREATE NONCLUSTERED INDEX [IX_Movies_FranchiseId] ON [dbo].[Movies]
(
	[FranchiseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MovieCharacters]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacters_Actors_ActorId] FOREIGN KEY([ActorId])
REFERENCES [dbo].[Actors] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacters] CHECK CONSTRAINT [FK_MovieCharacters_Actors_ActorId]
GO
ALTER TABLE [dbo].[MovieCharacters]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacters_Characters_CharacterId] FOREIGN KEY([CharacterId])
REFERENCES [dbo].[Characters] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacters] CHECK CONSTRAINT [FK_MovieCharacters_Characters_CharacterId]
GO
ALTER TABLE [dbo].[MovieCharacters]  WITH CHECK ADD  CONSTRAINT [FK_MovieCharacters_Movies_MovieId] FOREIGN KEY([MovieId])
REFERENCES [dbo].[Movies] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MovieCharacters] CHECK CONSTRAINT [FK_MovieCharacters_Movies_MovieId]
GO
ALTER TABLE [dbo].[Movies]  WITH CHECK ADD  CONSTRAINT [FK_Movies_Franchises_FranchiseId] FOREIGN KEY([FranchiseId])
REFERENCES [dbo].[Franchises] ([Id])
GO
ALTER TABLE [dbo].[Movies] CHECK CONSTRAINT [FK_Movies_Franchises_FranchiseId]
GO
USE [master]
GO
ALTER DATABASE [MovieCharacterDB] SET  READ_WRITE 
GO
