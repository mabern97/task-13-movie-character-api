Project details contained in: Task 13.pdf

Solution contains:
- Swagger Documentation
- Postman Collections
- Repository Pattern
- DTO Abstraction