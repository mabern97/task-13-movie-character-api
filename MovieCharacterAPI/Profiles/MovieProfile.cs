﻿using AutoMapper;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieShortDTO>();
            CreateMap<Movie, MovieDTO>();

            CreateMap<Movie, MovieActorDTO>().ForMember(m => m.Actors, opt => opt.MapFrom(mc => mc.MovieCharacters.Select(mc => mc.Actor).Distinct().ToList()));
            CreateMap<Movie, MovieCharacterDTO>().ForMember(m => m.Characters, opt => opt.MapFrom(mc => mc.MovieCharacters.Select(mc => mc.Character).Distinct().ToList()));
        }
    }
}
