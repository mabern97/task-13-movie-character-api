﻿using AutoMapper;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDTO>();

            /*
             * Map all the movies in a franchise
             */
            CreateMap<Franchise, FranchiseMoviesDTO>()
                .ForMember(f => f.Movies, opt => opt
                    .MapFrom(m => m.HasMovies
                        .Distinct().ToList()));

            /* Distinctly map all actors in a franchise
             * Franchise
             *      Movies
             *          MovieCharacters
             *              SELECT: Actor
             */
            CreateMap<Franchise, FranchiseActorsDTO>()
                .ForMember(f => f.Actors, opt => opt
                    .MapFrom(m => m.HasMovies
                        .SelectMany(mc => mc.MovieCharacters)
                        .Select(mc => mc.Actor)
                        .Distinct().ToList()));

            /* Distinctly map all characters in a franchise
             * Franchise
             *      Movies
             *          MovieCharacters
             *              SELECT: Character
             */
            CreateMap<Franchise, FranchiseCharactersDTO>()
                .ForMember(f => f.Characters, opt => opt
                    .MapFrom(m => m.HasMovies
                        .SelectMany(mc => mc.MovieCharacters)
                        .Select(mc => mc.Character)
                        .Distinct().ToList()));

            /* Distinctly map everything in a franchise
             * Franchise
             *      Movies
             *          MovieCharacters
             *              SELECT: *
             */
            CreateMap<Franchise, FranchiseFullDTO>()
                // Map movies
                .ForMember(f => f.Movies, opt => opt
                    .MapFrom(m => m.HasMovies
                        .Distinct().ToList()))
                // Map actors
                .ForMember(f => f.Actors, opt => opt
                    .MapFrom(m => m.HasMovies
                        .SelectMany(mc => mc.MovieCharacters)
                        .Select(mc => mc.Actor)
                        .Distinct().ToList()))
                // Map characters
                .ForMember(f => f.Characters, opt => opt
                    .MapFrom(m => m.HasMovies
                        .SelectMany(mc => mc.MovieCharacters)
                        .Select(mc => mc.Character)
                        .Distinct().ToList()));

        }
    }
}
