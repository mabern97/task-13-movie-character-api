﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor, ActorShortDTO>(); // Map the 'Actor' model to the ActorShort Data Transfer Object
            CreateMap<Actor, ActorDTO>(); // Map the 'Actor' model to the ActorDTO Data Transfer Object

            // For each Movie an Actor has, 
            /* Complex Mapping:
             *  - ActorMovieDTO: Contains MovieDTO, Select each Movie from the Movies property (MovieCharacters) and Distinctly list them
             *  - ActorCharacterDTO: Contains CharacterDTO, Select each Character from the Movies property (MovieCharacters) and Distinctly list them
             *  - ActorFullDTO
            */
            CreateMap<Actor, ActorMovieDTO>()
                .ForMember(am => am.Movies, opt => opt
                    .MapFrom(mc => mc.InMovies
                        .Select(mc => mc.Movie)
                        .Distinct().ToList()));

            CreateMap<Actor, ActorCharacterDTO>()
                .ForMember(ac => ac.Characters, opt => opt
                    .MapFrom(mc => mc.InMovies
                        .Select(mc => mc.Character)
                        .Distinct().ToList()));

            CreateMap<Actor, ActorFullDTO>()
                // Retrieve all movies that a actor has played in
                .ForMember(af => af.Movies, opt => opt
                    .MapFrom(mc => mc.InMovies
                        .Select(mc => mc.Movie)
                        .Distinct().ToList()))
                // Retrieve all characters that an actor has played as
                .ForMember(af => af.Characters, opt => opt
                    .MapFrom(mc => mc.InMovies
                        .Select(mc => mc.Character)
                        .Distinct().ToList()));
        }
    }
}
