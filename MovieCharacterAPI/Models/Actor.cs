﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Models
{
    public enum Gender
    {
        Male,
        Female
    }

    public class Actor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public DateTime DOB { get; set; }
        public string PlaceOfBirth { get; set; }

        public string Biography { get; set; }
        public string Picture { get; set; }
        public IEnumerable<MovieCharacter> InMovies { get; set; }

        public Actor()
        {
            InMovies = new List<MovieCharacter>();
        }
    }
}
