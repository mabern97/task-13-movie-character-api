﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public DateTime ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; } // URL
        public string Trailer { get; set; } // URL - YouTube?
        public IEnumerable<MovieCharacter> MovieCharacters { get; set; }
        // One-to-Many; Franchise, Many Movies
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
