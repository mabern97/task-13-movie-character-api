﻿using MovieCharacterAPI.Context;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Runtime.CompilerServices;

namespace MovieCharacterAPI.Repositories
{
    public class ActorRepository : Repository<Actor, ApplicationDbContext>
    {
        public IQueryable<Actor> Queryable
        {
            get { return Context.Actors.AsQueryable(); }
        }

        public ActorRepository(DbContextOptions options)
            :base(options)
        {
        }
        public override void Delete(Actor obj)
        {
            Context.Actors.Remove(obj);
            Save();
        }

        public override Actor Get(Expression<Func<Actor, bool>> predicate)
        {
            Actor actor = Context.Actors.Where(predicate).SingleOrDefault();

            return actor;
        }

        public override async Task<Actor> GetAsync(Expression<Func<Actor, bool>> predicate)
        {
            Actor actor = await Context.Actors.Where(predicate).SingleOrDefaultAsync();

            return actor;
        }

        public override Actor GetById(int id)
        {
            return Context.Actors.Find(id);
        }

        public async override Task<Actor> GetByIdAsync(int id)
        {
            Actor actor = await Context.Actors.FindAsync(id);

            return actor;
        }

        public override IEnumerable<Actor> GetAll()
        {
            return Context.Actors.ToList();
        }

        public async override Task<IEnumerable<Actor>> GetAllAsync()
        {
            IEnumerable<Actor> actors = await Context.Actors.ToListAsync();

            return actors;
        }

        public override Actor Insert(Actor obj)
        {
            Actor savedObject = Context.Actors.Add(obj).Entity;
            Save();

            return savedObject;
        }

        public async override Task<Actor> InsertAsync(Actor obj)
        {
            var savedObject = await Context.Actors.AddAsync(obj);
            await SaveAsync();

            return savedObject.Entity;
        }

        public override void Update(Actor obj)
        {
            Context.Entry(obj).State = EntityState.Modified;
            Save();
        }

        public override int Save()
        {
            return Context.SaveChanges();
        }

        public async override Task<int> SaveAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public bool Exists(int id)
        {
            return Context.Actors.Any(a => a.Id == id);
        }
    }
}
