﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MovieCharacterAPI.Repositories
{
    public interface IRepository<T>
    {
        T Insert(T obj); // Create
        Task<T> InsertAsync(T obj); // Async Create
        T GetById(int id); // Read by Id
        Task<T> GetByIdAsync(int id); // Async Read by Id
        T Get(Expression<Func<T, bool>> predicate); // Read
        Task<T> GetAsync(Expression<Func<T, bool>> predicate); // Async Read
        IEnumerable<T> GetAll(); // Read
        Task<IEnumerable<T>> GetAllAsync(); // Async Read
        void Update(T obj); // Update
        void Delete(T obj); // Delete
        int Save(); // Save
        Task<int> SaveAsync(); // Async Save
    }
}
