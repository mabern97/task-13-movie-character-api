﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Repositories
{
    public abstract class Repository<TType, TContext> : IRepository<TType>
    {
        protected TContext Context;

        public Repository(DbContextOptions options)
        {
            Type type = typeof(TContext);
            
            Context = (TContext)Activator.CreateInstance(type, options);
        }

        public abstract void Delete(TType obj);
        public abstract TType Get(Expression<Func<TType, bool>> predicate);
        public abstract IEnumerable<TType> GetAll();
        public abstract Task<IEnumerable<TType>> GetAllAsync();
        public abstract Task<TType> GetAsync(Expression<Func<TType, bool>> predicate);
        public abstract TType GetById(int id);
        public abstract Task<TType> GetByIdAsync(int id);
        public abstract TType Insert(TType obj);
        public abstract Task<TType> InsertAsync(TType obj);
        public abstract int Save();
        public abstract Task<int> SaveAsync();
        public abstract void Update(TType obj);
    }
}
