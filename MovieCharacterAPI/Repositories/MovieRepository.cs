﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Context;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Repositories
{
    public class MovieRepository : Repository<Movie, ApplicationDbContext>
    {
        public IQueryable<Movie> Queryable
        {
            get { return Context.Movies.AsQueryable(); }
        }
        public MovieRepository(DbContextOptions options)
            :base(options)
        {

        }

        public override void Delete(Movie obj)
        {
            Context.Movies.Remove(obj);
        }

        public override Movie Get(Expression<Func<Movie, bool>> predicate)
        {
            Movie movie = Context.Movies.Where(predicate).SingleOrDefault();

            return movie;
        }

        public override async Task<Movie> GetAsync(Expression<Func<Movie, bool>> predicate)
        {
            Movie movie = await Context.Movies.Where(predicate).SingleOrDefaultAsync();

            return movie;
        }

        public override Movie GetById(int id)
        {
            return Context.Movies.Find(id);
        }

        public async override Task<Movie> GetByIdAsync(int id)
        {
            Movie movie = await Context.Movies.FindAsync(id);

            return movie;
        }

        public override IEnumerable<Movie> GetAll()
        {
            return Context.Movies.ToList();
        }

        public async override Task<IEnumerable<Movie>> GetAllAsync()
        {
            IEnumerable<Movie> movies = await Context.Movies.ToListAsync();

            return movies;
        }

        public override Movie Insert(Movie obj)
        {
            Movie savedObject = Context.Movies.Add(obj).Entity;
            Save();

            return savedObject;
        }

        public async override Task<Movie> InsertAsync(Movie obj)
        {
            var savedObject = await Context.Movies.AddAsync(obj);
            await SaveAsync();

            return savedObject.Entity;
        }

        public override void Update(Movie obj)
        {
            Context.Entry(obj).State = EntityState.Modified;
        }

        public override int Save()
        {
            return Context.SaveChanges();
        }

        public async override Task<int> SaveAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public bool Exists(int id)
        {
            return Context.Movies.Any(a => a.Id == id);
        }
    }
}
