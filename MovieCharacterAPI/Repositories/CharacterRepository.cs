﻿using MovieCharacterAPI.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharacterAPI.Models;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace MovieCharacterAPI.Repositories
{
    public class CharacterRepository : Repository<Character, ApplicationDbContext>
    {
        public IQueryable<Character> Queryable
        {
            get { return Context.Characters.AsQueryable(); }
        }
        public CharacterRepository(DbContextOptions options)
            :base(options)
        {

        }
        public override void Delete(Character obj)
        {
            Context.Characters.Remove(obj);
            Save();
        }

        public bool Exists(int id)
        {
            return Context.Characters.Any(c => c.Id == id);
        }

        public override Character Get(Expression<Func<Character, bool>> predicate)
        {
            Character character = Context.Characters.Where(predicate).SingleOrDefault();

            return character;
        }

        public override IEnumerable<Character> GetAll()
        {
            return Context.Characters.ToList();
        }

        public async override Task<IEnumerable<Character>> GetAllAsync()
        {
            IEnumerable<Character> characters = await Context.Characters.ToListAsync();

            return characters;
        }

        public async override Task<Character> GetAsync(Expression<Func<Character, bool>> predicate)
        {
            Character character = await Context.Characters.Where(predicate).SingleOrDefaultAsync();

            return character;
        }

        public override Character GetById(int id)
        {
            return Context.Characters.Find(id);
        }

        public async override Task<Character> GetByIdAsync(int id)
        {
            Character character = await Context.Characters.FindAsync(id);

            return character;
        }

        public override Character Insert(Character obj)
        {
            Character savedObject = Context.Characters.Add(obj).Entity;
            Save();

            return savedObject;
        }

        public async override Task<Character> InsertAsync(Character obj)
        {
            var savedObject = await Context.Characters.AddAsync(obj);
            await SaveAsync();

            return savedObject.Entity;
        }

        public override int Save()
        {
            return Context.SaveChanges();
        }

        public async override Task<int> SaveAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public override void Update(Character obj)
        {
            Context.Entry(obj).State = EntityState.Modified;
            Save();
        }
    }
}
