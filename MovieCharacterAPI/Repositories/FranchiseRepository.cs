﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Context;

namespace MovieCharacterAPI.Repositories
{
    public class FranchiseRepository : Repository<Franchise, ApplicationDbContext>
    {
        public IQueryable<Franchise> Queryable
        {
            get { return Context.Franchises.AsQueryable(); }
        }
        public FranchiseRepository(DbContextOptions options)
            :base(options)
        {

        }
        public override void Delete(Franchise obj)
        {
            Context.Franchises.Remove(obj);
            Save();
        }

        public bool Exists(int id)
        {
            return Context.Franchises.Any(c => c.Id == id);
        }

        public override Franchise Get(Expression<Func<Franchise, bool>> predicate)
        {
            Franchise character = Context.Franchises.Where(predicate).SingleOrDefault();

            return character;
        }

        public override IEnumerable<Franchise> GetAll()
        {
            return Context.Franchises.ToList();
        }

        public async override Task<IEnumerable<Franchise>> GetAllAsync()
        {
            IEnumerable<Franchise> characters = await Context.Franchises.ToListAsync();

            return characters;
        }

        public async override Task<Franchise> GetAsync(Expression<Func<Franchise, bool>> predicate)
        {
            Franchise character = await Context.Franchises.Where(predicate).SingleOrDefaultAsync();

            return character;
        }

        public override Franchise GetById(int id)
        {
            return Context.Franchises.Find(id);
        }

        public async override Task<Franchise> GetByIdAsync(int id)
        {
            Franchise character = await Context.Franchises.FindAsync(id);

            return character;
        }

        public override Franchise Insert(Franchise obj)
        {
            Franchise savedObject = Context.Franchises.Add(obj).Entity;
            Save();

            return savedObject;
        }

        public async override Task<Franchise> InsertAsync(Franchise obj)
        {
            var savedObject = await Context.Franchises.AddAsync(obj);
            await SaveAsync();

            return savedObject.Entity;
        }

        public override int Save()
        {
            return Context.SaveChanges();
        }

        public async override Task<int> SaveAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public override void Update(Franchise obj)
        {
            Context.Entry(obj).State = EntityState.Modified;
            Save();
        }
    }
}
