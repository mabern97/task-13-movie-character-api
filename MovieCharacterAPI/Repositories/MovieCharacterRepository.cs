﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Context;

namespace MovieCharacterAPI.Repositories
{
    public class MovieCharacterRepository : Repository<MovieCharacter, ApplicationDbContext>
    {
        public IQueryable<MovieCharacter> Queryable
        {
            get { return Context.MovieCharacters.AsQueryable(); }
        }
        public MovieCharacterRepository(DbContextOptions options)
            : base(options)
        {

        }
        public override void Delete(MovieCharacter obj)
        {
            Context.MovieCharacters.Remove(obj);
            Save();
        }

        public bool Exists(int movieId, int characterId)
        {
            return Context.MovieCharacters.Any(n => n.MovieId == movieId && n.CharacterId == characterId);
        }

        public override MovieCharacter Get(Expression<Func<MovieCharacter, bool>> predicate)
        {
            MovieCharacter character = Context.MovieCharacters.Where(predicate).SingleOrDefault();

            return character;
        }

        public override IEnumerable<MovieCharacter> GetAll()
        {
            return Context.MovieCharacters.ToList();
        }

        public async override Task<IEnumerable<MovieCharacter>> GetAllAsync()
        {
            IEnumerable<MovieCharacter> characters = await Context.MovieCharacters.ToListAsync();

            return characters;
        }

        public async override Task<MovieCharacter> GetAsync(Expression<Func<MovieCharacter, bool>> predicate)
        {
            MovieCharacter character = await Context.MovieCharacters.Where(predicate).SingleOrDefaultAsync();

            return character;
        }

        public override MovieCharacter GetById(int id)
        {
            return Context.MovieCharacters.Find(id);
        }

        public async override Task<MovieCharacter> GetByIdAsync(int id)
        {
            MovieCharacter character = await Context.MovieCharacters.FindAsync(id);

            return character;
        }

        public override MovieCharacter Insert(MovieCharacter obj)
        {
            MovieCharacter savedObject = Context.MovieCharacters.Add(obj).Entity;
            Save();

            return savedObject;
        }

        public async override Task<MovieCharacter> InsertAsync(MovieCharacter obj)
        {
            var savedObject = await Context.MovieCharacters.AddAsync(obj);
            await SaveAsync();

            return savedObject.Entity;
        }

        public override int Save()
        {
            return Context.SaveChanges();
        }

        public async override Task<int> SaveAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public override void Update(MovieCharacter obj)
        {
            Context.Entry(obj).State = EntityState.Modified;
            Save();
        }
    }
}
