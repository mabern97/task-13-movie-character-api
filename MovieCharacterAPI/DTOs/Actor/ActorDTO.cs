﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.DTOs
{
    public class ActorDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }

        public DateTime DOB { get; set; }
        public string PlaceOfBirth { get; set; }

        public string Biography { get; set; }
        public string Picture { get; set; }
    }
}
