﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.DTOs
{
    public class ActorMovieDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public IEnumerable<MovieDTO> Movies { get; set; }
    }
}
