﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs
{
    public class FranchiseFullDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<ActorCharacterDTO> Actors { get; set; }
        public IEnumerable<CharacterDTO> Characters { get; set; }
        public IEnumerable<MovieActorDTO> Movies { get; set; }
    }
}
