﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.DTOs
{
    public class MovieActorDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public DateTime ReleaseYear { get; set; }
        public IEnumerable<ActorShortDTO> Actors { get; set; }
    }
}
