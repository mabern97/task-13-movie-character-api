﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class MovieDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    DOB = table.Column<DateTime>(nullable: false),
                    PlaceOfBirth = table.Column<string>(nullable: true),
                    Biography = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    Alias = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    ReleaseYear = table.Column<DateTime>(nullable: false),
                    Director = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    Trailer = table.Column<string>(nullable: true),
                    FranchiseId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacters",
                columns: table => new
                {
                    MovieId = table.Column<int>(nullable: false),
                    CharacterId = table.Column<int>(nullable: false),
                    ActorId = table.Column<int>(nullable: false),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacters", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Actors_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DOB", "FirstName", "Gender", "LastName", "MiddleName", "Picture", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 1, "American actor, producer, and singer. His career has been characterized by critical and popular success in his youth, followed by a period of substance abuse and legal troubles, before a resurgence of commercial success in middle age.", new DateTime(1965, 4, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Robert", 0, "Downey Jr.", "John", "https://m.media-amazon.com/images/M/MV5BNzg1MTUyNDYxOF5BMl5BanBnXkFtZTgwNTQ4MTE2MjE@._V1_UX214_CR0,0,214,317_AL_.jpg", "New York City, New York, USA" },
                    { 13, "Bio12", new DateTime(1968, 2, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Josh", 0, "Brolin", null, "https://m.media-amazon.com/images/M/MV5BNTAzMzA3NjQwOF5BMl5BanBnXkFtZTgwMDUzODQ5MTI@._V1_UY317_CR23,0,214,317_AL_.jpg", "Kingston upon Thames, England, UK" },
                    { 12, "Bio12", new DateTime(1996, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tom", 0, "Holland", "Stanley", "https://m.media-amazon.com/images/M/MV5BNTAzMzA3NjQwOF5BMl5BanBnXkFtZTgwMDUzODQ5MTI@._V1_UY317_CR23,0,214,317_AL_.jpg", "Kingston upon Thames, England, UK" },
                    { 11, "Bio11", new DateTime(1989, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Brie", 1, "Larson", null, "https://m.media-amazon.com/images/M/MV5BMjExODkxODU3NF5BMl5BanBnXkFtZTgwNTM0MTk3NjE@._V1_UY317_CR7,0,214,317_AL_.jpg", "Sacramento, California, USA" },
                    { 9, "Bio9", new DateTime(1976, 7, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Benedict", 0, "Cumberbatch", "Timophy", "https://m.media-amazon.com/images/M/MV5BMjE0MDkzMDQwOF5BMl5BanBnXkFtZTgwOTE1Mjg1MzE@._V1_UY317_CR2,0,214,317_AL_.jpg", "Passaic, New Jersey, USA" },
                    { 8, "Bio8", new DateTime(1969, 4, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Paul", 0, "Rudd", "Stephen", "https://m.media-amazon.com/images/M/MV5BMTY4NTEwNDg1MV5BMl5BanBnXkFtZTgwODMwMDA0ODE@._V1_UY317_CR20,0,214,317_AL_.jpg", "Passaic, New Jersey, USA" },
                    { 10, "Bio10", new DateTime(1976, 11, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), "Chadwick", 0, "Boseman", null, "https://m.media-amazon.com/images/M/MV5BMTk2OTY5MzcwMV5BMl5BanBnXkFtZTgwODM4MDI5MjI@._V1_UX214_CR0,0,214,317_AL_.jpg", "Passaic, New Jersey, USA" },
                    { 6, "Bio6", new DateTime(1971, 1, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jeremy", 0, "Renner", "Lee", "https://m.media-amazon.com/images/M/MV5BOTk2NDc2ODgzMF5BMl5BanBnXkFtZTcwMTMzOTQ4Nw@@._V1_UX214_CR0,0,214,317_AL_.jpg", "Modesto, California, USA" },
                    { 5, "Bio5", new DateTime(1984, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Scarlett", 1, "Johansson", null, "https://m.media-amazon.com/images/M/MV5BMTM3OTUwMDYwNl5BMl5BanBnXkFtZTcwNTUyNzc3Nw@@._V1_UY317_CR23,0,214,317_AL_.jpg", "New York City, New York, USA" },
                    { 4, "Bio4", new DateTime(1983, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Chris", 0, "Hemsworth", null, "https://m.media-amazon.com/images/M/MV5BOTU2MTI0NTIyNV5BMl5BanBnXkFtZTcwMTA4Nzc3OA@@._V1_UX214_CR0,0,214,317_AL_.jpg", "Melbourne, Victoria, Australia" },
                    { 3, "Bio3", new DateTime(1967, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mark", 0, "Ruffalo", null, "https://m.media-amazon.com/images/M/MV5BNDQyNzMzZTMtYjlkNS00YzFhLWFhMTctY2M4YmQ1NmRhODBkXkEyXkFqcGdeQXVyNjcyNzgyOTE@._V1_UY317_CR20,0,214,317_AL_.jpg", "Kenosha, Wisconsin, USA" },
                    { 2, "Bio2", new DateTime(1981, 6, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "Chris", 0, "Evans", "Robert", "https://m.media-amazon.com/images/M/MV5BMTU2NTg1OTQzMF5BMl5BanBnXkFtZTcwNjIyMjkyMg@@._V1_UY317_CR6,0,214,317_AL_.jpg", "Boston, Massachusetts, USA" },
                    { 7, "Bio7", new DateTime(1964, 11, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), "Don", 0, "Cheadle", "Frank", "https://m.media-amazon.com/images/M/MV5BNDMxNDM3MzY5N15BMl5BanBnXkFtZTcwMjkzOTY4MQ@@._V1_UY317_CR17,0,214,317_AL_.jpg", "Kansas City, Missouri, USA" }
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 8, "Ant-Man", "Scott Lang", 0, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 12, "Spider-man", "Peter Parker", 0, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 11, "Captain Marvel", "Carol Danvers", 1, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 10, "Black Panther", "King T'Challa", 0, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 9, "Doctor Strange", "Stephen Strange", 0, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 7, "War Machine", "James Rhodes", 0, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 1, "Iron Man", "Tony Stark", 0, "https://i.pinimg.com/736x/37/7f/e3/377fe3095549bed0cef8b50d3bbd27cd.jpg" },
                    { 5, "Black Widow", "Natasha Romanoff", 1, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 4, "Thor", "Thor Odinson", 0, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 3, "Incredible Hulk", "Steve Bannon", 0, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 2, "Captain America", "Steve Rogers", 0, "https://vignette.wikia.nocookie.net/earth-199999/images/7/72/CapAmerica.jpg/revision/latest/top-crop/width/360/height/450?cb=20180816180257" },
                    { 13, "Thanos", "Thanos", 0, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" },
                    { 6, "Hawkeye", "Clint Barton", 0, "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { 1, "Better universe than DC tbh", "Marvel Cinematic Universe" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Anthony Russo, Joe Russo", 1, "Action,Adventure,Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMjMxNjY2MDU1OV5BMl5BanBnXkFtZTgwNzY1MTUwNTM@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2018, 4, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Avengers: Infinity War", "https://www.youtube.com/watch?v=6ZfuNTqbHE8" },
                    { 2, "Anthony Russo, Joe Russo", 1, "Action,Adventure,Drama", "https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2019, 4, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), "Avengers: Endgame", "https://www.youtube.com/watch?v=TcMBFSGVi1c" },
                    { 3, "Jon Favreau", 1, "Action,Adventure,Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2008, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Iron Man", "https://www.youtube.com/watch?v=8ugaeA-nMTc" },
                    { 4, "Jon Favreau", 1, "Action,Adventure,Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMTM0MDgwNjMyMl5BMl5BanBnXkFtZTcwNTg3NzAzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2010, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Iron Man 2", "https://www.youtube.com/watch?v=BoohRoVA9WQ" },
                    { 5, "Shane Black", 1, "Action,Adventure,Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMjE5MzcyNjk1M15BMl5BanBnXkFtZTcwMjQ4MjcxOQ@@._V1_UY268_CR3,0,182,268_AL_.jpg", new DateTime(2013, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Iron Man 3", "https://www.youtube.com/watch?v=Ke1Y3P9D0Bc" },
                    { 6, "Joe Johnston", 1, "Action,Adventure,Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMTYzOTc2NzU3N15BMl5BanBnXkFtZTcwNjY3MDE3NQ@@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2011, 7, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Captain America: The First Avenger", "https://www.youtube.com/watch?v=JerVrbLldXw" },
                    { 7, "Anthony Russo, Joe Russo", 1, "Action,Adventure,Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMzA2NDkwODAwM15BMl5BanBnXkFtZTgwODk5MTgzMTE@._V1_UY268_CR1,0,182,268_AL_.jpg", new DateTime(2014, 4, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Captain America: The Winter Soldier", "https://www.youtube.com/watch?v=7SlILk2WMTI" },
                    { 8, "Anthony Russo, Joe Russo", 1, "Action,Adventure,Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMjQ0MTgyNjAxMV5BMl5BanBnXkFtZTgwNjUzMDkyODE@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2016, 5, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Captain America: Civil War", "https://www.youtube.com/watch?v=dKrVegVI0Us" },
                    { 9, "Joe Whedon", 1, "Action,Adventure,Sci-Fi", "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2012, 5, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Avengers", "https://www.youtube.com/watch?v=eOrNdBpGMv8" },
                    { 10, "Joe Whedon", 1, "Action,Adventure,Sci-Fi", "https://m.media-amazon.com/images/M/MV5BMTM4OGJmNWMtOTM4Ni00NTE3LTg3MDItZmQxYjc4N2JhNmUxXkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_UX182_CR0,0,182,268_AL_.jpg", new DateTime(2015, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Avengers: Age of Ultron", "https://www.youtube.com/watch?v=tmeOjFno6Do" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "MovieId", "CharacterId", "ActorId", "Picture" },
                values: new object[,]
                {
                    { 1, 1, 1, null },
                    { 2, 9, 9, null },
                    { 2, 10, 10, null },
                    { 2, 11, 11, null },
                    { 2, 12, 12, null },
                    { 2, 13, 13, null },
                    { 3, 1, 1, null },
                    { 3, 5, 5, null },
                    { 2, 8, 8, null },
                    { 4, 1, 1, null },
                    { 5, 1, 1, null },
                    { 6, 2, 2, null },
                    { 7, 2, 2, null },
                    { 7, 5, 5, null },
                    { 8, 1, 1, null },
                    { 8, 2, 2, null },
                    { 8, 5, 5, null },
                    { 4, 5, 5, null },
                    { 8, 12, 12, null },
                    { 2, 7, 7, null },
                    { 2, 5, 5, null },
                    { 1, 2, 2, null },
                    { 1, 3, 3, null },
                    { 1, 4, 4, null },
                    { 1, 5, 5, null },
                    { 1, 6, 6, null },
                    { 1, 7, 7, null },
                    { 1, 8, 8, null },
                    { 2, 6, 6, null },
                    { 1, 9, 9, null },
                    { 1, 11, 11, null },
                    { 1, 12, 12, null },
                    { 1, 13, 13, null },
                    { 2, 1, 1, null },
                    { 2, 2, 2, null },
                    { 2, 3, 3, null },
                    { 2, 4, 4, null },
                    { 1, 10, 10, null },
                    { 8, 8, 8, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_ActorId",
                table: "MovieCharacters",
                column: "ActorId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacters",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacters");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
