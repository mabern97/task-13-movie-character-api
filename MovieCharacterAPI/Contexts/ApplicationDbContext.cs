﻿using System;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;

using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Context
{
    public class ApplicationDbContext : DbContext
    {
        public virtual DbSet<Actor> Actors { get; set; }
        public virtual DbSet<Character> Characters { get; set; }
        public virtual DbSet<MovieCharacter> MovieCharacters { get; set; }
        public virtual DbSet<Movie> Movies { get; set; }
        public virtual DbSet<Franchise> Franchises { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            :base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId }); // Composite Key

            // Actors
            Actor robert = new Actor()
            {
                Id = 1,
                FirstName = "Robert",
                MiddleName = "John",
                LastName = "Downey Jr.",
                Gender = Gender.Male,
                DOB = new DateTime(1965, 4, 4), // April 4th, 1965
                PlaceOfBirth = "New York City, New York, USA",
                Biography = "American actor, producer, and singer. His career has been characterized by critical and popular success in his youth, followed by a period of substance abuse and legal troubles, before a resurgence of commercial success in middle age.",
                Picture = "https://m.media-amazon.com/images/M/MV5BNzg1MTUyNDYxOF5BMl5BanBnXkFtZTgwNTQ4MTE2MjE@._V1_UX214_CR0,0,214,317_AL_.jpg"
            };

            Actor chrisA = new Actor()
            {
                Id = 2,
                FirstName = "Chris",
                MiddleName = "Robert",
                LastName = "Evans",
                Gender = Gender.Male,
                DOB = new DateTime(1981, 6, 13), // June 13th, 1981
                PlaceOfBirth = "Boston, Massachusetts, USA",
                Biography = "Bio2",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTU2NTg1OTQzMF5BMl5BanBnXkFtZTcwNjIyMjkyMg@@._V1_UY317_CR6,0,214,317_AL_.jpg"
            };

            Actor mark = new Actor()
            {
                Id = 3,
                FirstName = "Mark",
                LastName = "Ruffalo",
                Gender = Gender.Male,
                DOB = new DateTime(1967, 11, 22), // November 22nd, 1967
                PlaceOfBirth = "Kenosha, Wisconsin, USA",
                Biography = "Bio3",
                Picture = "https://m.media-amazon.com/images/M/MV5BNDQyNzMzZTMtYjlkNS00YzFhLWFhMTctY2M4YmQ1NmRhODBkXkEyXkFqcGdeQXVyNjcyNzgyOTE@._V1_UY317_CR20,0,214,317_AL_.jpg"
            };

            Actor chrisB = new Actor()
            {
                Id = 4,
                FirstName = "Chris",
                LastName = "Hemsworth",
                Gender = Gender.Male,
                DOB = new DateTime(1983, 8, 11), // August 11th, 1983
                PlaceOfBirth = "Melbourne, Victoria, Australia",
                Biography = "Bio4",
                Picture = "https://m.media-amazon.com/images/M/MV5BOTU2MTI0NTIyNV5BMl5BanBnXkFtZTcwMTA4Nzc3OA@@._V1_UX214_CR0,0,214,317_AL_.jpg"
            };

            Actor scarlett = new Actor()
            {
                Id = 5,
                FirstName = "Scarlett",
                LastName = "Johansson",
                Gender = Gender.Female,
                DOB = new DateTime(1984, 11, 22), // November 22nd, 1984
                PlaceOfBirth = "New York City, New York, USA",
                Biography = "Bio5",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTM3OTUwMDYwNl5BMl5BanBnXkFtZTcwNTUyNzc3Nw@@._V1_UY317_CR23,0,214,317_AL_.jpg"
            };

            Actor jeremy = new Actor()
            {
                Id = 6,
                FirstName = "Jeremy",
                MiddleName = "Lee",
                LastName = "Renner",
                Gender = Gender.Male,
                DOB = new DateTime(1971, 1, 7), // January 7th, 1971
                PlaceOfBirth = "Modesto, California, USA",
                Biography = "Bio6",
                Picture = "https://m.media-amazon.com/images/M/MV5BOTk2NDc2ODgzMF5BMl5BanBnXkFtZTcwMTMzOTQ4Nw@@._V1_UX214_CR0,0,214,317_AL_.jpg"
            };

            Actor don = new Actor()
            {
                Id = 7,
                FirstName = "Don",
                MiddleName = "Frank",
                LastName = "Cheadle",
                Gender = Gender.Male,
                DOB = new DateTime(1964, 11, 29), // November 29th, 1964
                PlaceOfBirth = "Kansas City, Missouri, USA",
                Biography = "Bio7",
                Picture = "https://m.media-amazon.com/images/M/MV5BNDMxNDM3MzY5N15BMl5BanBnXkFtZTcwMjkzOTY4MQ@@._V1_UY317_CR17,0,214,317_AL_.jpg"
            };

            Actor paul = new Actor()
            {
                Id = 8,
                FirstName = "Paul",
                MiddleName = "Stephen",
                LastName = "Rudd",
                Gender = Gender.Male,
                DOB = new DateTime(1969, 4, 6), // April 6th, 1969
                PlaceOfBirth = "Passaic, New Jersey, USA",
                Biography = "Bio8",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTY4NTEwNDg1MV5BMl5BanBnXkFtZTgwODMwMDA0ODE@._V1_UY317_CR20,0,214,317_AL_.jpg"
            };

            Actor benedict = new Actor()
            {
                Id = 9,
                FirstName = "Benedict",
                MiddleName = "Timophy",
                LastName = "Cumberbatch",
                Gender = Gender.Male,
                DOB = new DateTime(1976, 7, 19), // June 19th, 1976
                PlaceOfBirth = "Passaic, New Jersey, USA",
                Biography = "Bio9",
                Picture = "https://m.media-amazon.com/images/M/MV5BMjE0MDkzMDQwOF5BMl5BanBnXkFtZTgwOTE1Mjg1MzE@._V1_UY317_CR2,0,214,317_AL_.jpg"
            };

            Actor chadwick = new Actor()
            {
                Id = 10,
                FirstName = "Chadwick",
                LastName = "Boseman",
                Gender = Gender.Male,
                DOB = new DateTime(1976, 11, 29), // November 29th, 1976
                PlaceOfBirth = "Passaic, New Jersey, USA",
                Biography = "Bio10",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTk2OTY5MzcwMV5BMl5BanBnXkFtZTgwODM4MDI5MjI@._V1_UX214_CR0,0,214,317_AL_.jpg"
            };

            Actor brie = new Actor()
            {
                Id = 11,
                FirstName = "Brie",
                LastName = "Larson",
                Gender = Gender.Female,
                DOB = new DateTime(1989, 10, 1), // October 1st, 1989
                PlaceOfBirth = "Sacramento, California, USA",
                Biography = "Bio11",
                Picture = "https://m.media-amazon.com/images/M/MV5BMjExODkxODU3NF5BMl5BanBnXkFtZTgwNTM0MTk3NjE@._V1_UY317_CR7,0,214,317_AL_.jpg"
            };

            Actor tom = new Actor()
            {
                Id = 12,
                FirstName = "Tom",
                MiddleName = "Stanley",
                LastName = "Holland",
                Gender = Gender.Male,
                DOB = new DateTime(1996, 6, 1), // June 1st, 1996
                PlaceOfBirth = "Kingston upon Thames, England, UK",
                Biography = "Bio12",
                Picture = "https://m.media-amazon.com/images/M/MV5BNTAzMzA3NjQwOF5BMl5BanBnXkFtZTgwMDUzODQ5MTI@._V1_UY317_CR23,0,214,317_AL_.jpg"
            };
            
            Actor josh = new Actor()
            {
                Id = 13,
                FirstName = "Josh",
                LastName = "Brolin",
                Gender = Gender.Male,
                DOB = new DateTime(1968, 2, 12), // February 12th, 1968
                PlaceOfBirth = "Kingston upon Thames, England, UK",
                Biography = "Bio12",
                Picture = "https://m.media-amazon.com/images/M/MV5BNTAzMzA3NjQwOF5BMl5BanBnXkFtZTgwMDUzODQ5MTI@._V1_UY317_CR23,0,214,317_AL_.jpg"
            };

            // Franchises
            Franchise mcu = new Franchise()
            {
                Id = 1,
                Name = "Marvel Cinematic Universe",
                Description = "Better universe than DC tbh"
            };

            // Movies
            Movie infinityWar = new Movie()
            {
                Id = 1,
                Title = "Avengers: Infinity War",
                Genre = "Action,Adventure,Sci-Fi",
                ReleaseYear = new DateTime(2018, 4, 27), // April 26th, 2019
                Director = "Anthony Russo, Joe Russo",
                Picture = "https://m.media-amazon.com/images/M/MV5BMjMxNjY2MDU1OV5BMl5BanBnXkFtZTgwNzY1MTUwNTM@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=6ZfuNTqbHE8",
                FranchiseId = 1
            };

            Movie endgame = new Movie()
            {
                Id = 2,
                Title = "Avengers: Endgame",
                Genre = "Action,Adventure,Drama",
                ReleaseYear = new DateTime(2019, 4, 26), // April 26th, 2019
                Director = "Anthony Russo, Joe Russo",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=TcMBFSGVi1c",
                FranchiseId = 1
            };

            Movie ironMan1 = new Movie()
            {
                Id = 3,
                Title = "Iron Man",
                Genre = "Action,Adventure,Sci-Fi",
                ReleaseYear = new DateTime(2008, 5, 2), // May 2nd, 2008
                Director = "Jon Favreau",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=8ugaeA-nMTc",
                FranchiseId = 1
            };

            Movie ironMan2 = new Movie()
            {
                Id = 4,
                Title = "Iron Man 2",
                Genre = "Action,Adventure,Sci-Fi",
                ReleaseYear = new DateTime(2010, 5, 7), // May 7th, 2010
                Director = "Jon Favreau",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTM0MDgwNjMyMl5BMl5BanBnXkFtZTcwNTg3NzAzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=BoohRoVA9WQ",
                FranchiseId = 1
            };

            Movie ironMan3 = new Movie()
            {
                Id = 5,
                Title = "Iron Man 3",
                Genre = "Action,Adventure,Sci-Fi",
                ReleaseYear = new DateTime(2013, 5, 3), // May 3rd, 2013
                Director = "Shane Black",
                Picture = "https://m.media-amazon.com/images/M/MV5BMjE5MzcyNjk1M15BMl5BanBnXkFtZTcwMjQ4MjcxOQ@@._V1_UY268_CR3,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=Ke1Y3P9D0Bc",
                FranchiseId = 1
            };

            Movie captainAmerica = new Movie()
            {
                Id = 6,
                Title = "Captain America: The First Avenger",
                Genre = "Action,Adventure,Sci-Fi",
                ReleaseYear = new DateTime(2011, 7, 22), // July 22nd, 2011
                Director = "Joe Johnston",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTYzOTc2NzU3N15BMl5BanBnXkFtZTcwNjY3MDE3NQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=JerVrbLldXw",
                FranchiseId = 1
            };

            Movie captainAmerica2 = new Movie()
            {
                Id = 7,
                Title = "Captain America: The Winter Soldier",
                Genre = "Action,Adventure,Sci-Fi",
                ReleaseYear = new DateTime(2014, 4, 4), // April 4th, 2014
                Director = "Anthony Russo, Joe Russo",
                Picture = "https://m.media-amazon.com/images/M/MV5BMzA2NDkwODAwM15BMl5BanBnXkFtZTgwODk5MTgzMTE@._V1_UY268_CR1,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=7SlILk2WMTI",
                FranchiseId = 1
            };

            Movie captainAmerica3 = new Movie()
            {
                Id = 8,
                Title = "Captain America: Civil War",
                Genre = "Action,Adventure,Sci-Fi",
                ReleaseYear = new DateTime(2016, 5, 6), // May 6th, 2016
                Director = "Anthony Russo, Joe Russo",
                Picture = "https://m.media-amazon.com/images/M/MV5BMjQ0MTgyNjAxMV5BMl5BanBnXkFtZTgwNjUzMDkyODE@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=dKrVegVI0Us",
                FranchiseId = 1
            };

            Movie avengers1 = new Movie()
            {
                Id = 9,
                Title = "The Avengers",
                Genre = "Action,Adventure,Sci-Fi",
                ReleaseYear = new DateTime(2012, 5, 4), // May 4th, 2012
                Director = "Joe Whedon",
                Picture = "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=eOrNdBpGMv8",
                FranchiseId = 1
            };

            Movie avengers2 = new Movie()
            {
                Id = 10,
                Title = "Avengers: Age of Ultron",
                Genre = "Action,Adventure,Sci-Fi",
                ReleaseYear = new DateTime(2015, 5, 1), // May 1st, 2015
                Director = "Joe Whedon",
                Picture = "https://m.media-amazon.com/images/M/MV5BMTM4OGJmNWMtOTM4Ni00NTE3LTg3MDItZmQxYjc4N2JhNmUxXkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_UX182_CR0,0,182,268_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=tmeOjFno6Do",
                FranchiseId = 1
            };

            // Characters
            Character ironMan = new Character()
            {
                Id = 1,
                FullName = "Tony Stark",
                Alias = "Iron Man",
                Gender = Gender.Male,
                Picture = "https://i.pinimg.com/736x/37/7f/e3/377fe3095549bed0cef8b50d3bbd27cd.jpg"
            };

            Character capnAmerica = new Character()
            {
                Id = 2,
                FullName = "Steve Rogers",
                Alias = "Captain America",
                Gender = Gender.Male,
                Picture = "https://vignette.wikia.nocookie.net/earth-199999/images/7/72/CapAmerica.jpg/revision/latest/top-crop/width/360/height/450?cb=20180816180257"
            };

            Character hulk = new Character()
            {
                Id = 3,
                FullName = "Steve Bannon",
                Alias = "Incredible Hulk",
                Gender = Gender.Male,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character thor = new Character()
            {
                Id = 4,
                FullName = "Thor Odinson",
                Alias = "Thor",
                Gender = Gender.Male,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character blackWidow = new Character()
            {
                Id = 5,
                FullName = "Natasha Romanoff",
                Alias = "Black Widow",
                Gender = Gender.Female,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character hawkeye = new Character()
            {
                Id = 6,
                FullName = "Clint Barton",
                Alias = "Hawkeye",
                Gender = Gender.Male,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character rhodes = new Character()
            {
                Id = 7,
                FullName = "James Rhodes",
                Alias = "War Machine",
                Gender = Gender.Male,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character antman = new Character()
            {
                Id = 8,
                FullName = "Scott Lang",
                Alias = "Ant-Man",
                Gender = Gender.Male,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character drStrange = new Character()
            {
                Id = 9,
                FullName = "Stephen Strange",
                Alias = "Doctor Strange",
                Gender = Gender.Male,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character blackPanther = new Character()
            {
                Id = 10,
                FullName = "King T'Challa",
                Alias = "Black Panther",
                Gender = Gender.Male,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character capnMarvel = new Character()
            {
                Id = 11,
                FullName = "Carol Danvers",
                Alias = "Captain Marvel",
                Gender = Gender.Female,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character spiderman = new Character()
            {
                Id = 12,
                FullName = "Peter Parker",
                Alias = "Spider-man",
                Gender = Gender.Male,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            Character thanos = new Character()
            {
                Id = 13,
                FullName = "Thanos",
                Alias = "Thanos",
                Gender = Gender.Male,
                Picture = "https://cdn.vox-cdn.com/thumbor/vjduTPE3tf58oi-GvZiwLJNMniE=/0x0:2048x1080/1200x800/filters:focal(1169x318:1495x644)/cdn.vox-cdn.com/uploads/chorus_image/image/63677017/DNR1520_v011_078137.1142.6.jpg"
            };

            // Seed Actors
            builder.Entity<Actor>().HasData(robert);
            builder.Entity<Actor>().HasData(chrisA);
            builder.Entity<Actor>().HasData(mark);
            builder.Entity<Actor>().HasData(chrisB);
            builder.Entity<Actor>().HasData(scarlett);
            builder.Entity<Actor>().HasData(jeremy);
            builder.Entity<Actor>().HasData(don);
            builder.Entity<Actor>().HasData(paul);
            builder.Entity<Actor>().HasData(benedict);
            builder.Entity<Actor>().HasData(chadwick);
            builder.Entity<Actor>().HasData(brie);
            builder.Entity<Actor>().HasData(tom);
            builder.Entity<Actor>().HasData(josh);

            // Seed Franchises
            builder.Entity<Franchise>().HasData(mcu);

            // Seed Movies
            builder.Entity<Movie>().HasData(infinityWar);
            builder.Entity<Movie>().HasData(endgame);

            builder.Entity<Movie>().HasData(ironMan1);
            builder.Entity<Movie>().HasData(ironMan2);
            builder.Entity<Movie>().HasData(ironMan3);
            builder.Entity<Movie>().HasData(captainAmerica);
            builder.Entity<Movie>().HasData(captainAmerica2);
            builder.Entity<Movie>().HasData(captainAmerica3);
            builder.Entity<Movie>().HasData(avengers1);
            builder.Entity<Movie>().HasData(avengers2);


            // Seed Characters
            builder.Entity<Character>().HasData(ironMan);
            builder.Entity<Character>().HasData(capnAmerica);
            builder.Entity<Character>().HasData(hulk);
            builder.Entity<Character>().HasData(thor);
            builder.Entity<Character>().HasData(blackWidow);
            builder.Entity<Character>().HasData(hawkeye);
            builder.Entity<Character>().HasData(rhodes);
            builder.Entity<Character>().HasData(antman);
            builder.Entity<Character>().HasData(drStrange);
            builder.Entity<Character>().HasData(blackPanther);
            builder.Entity<Character>().HasData(capnMarvel);
            builder.Entity<Character>().HasData(spiderman);
            builder.Entity<Character>().HasData(thanos);

            // Seed Movie Character Relationships

                // Infinity War
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 1, CharacterId = 1 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 2, CharacterId = 2 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 3, CharacterId = 3 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 4, CharacterId = 4 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 5, CharacterId = 5 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 6, CharacterId = 6 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 7, CharacterId = 7 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 8, CharacterId = 8 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 9, CharacterId = 9 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 10, CharacterId = 10 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 11, CharacterId = 11 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 12, CharacterId = 12 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 1, ActorId = 13, CharacterId = 13 });

                // End Game
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 1, CharacterId = 1 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 2, CharacterId = 2 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 3, CharacterId = 3 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 4, CharacterId = 4 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 5, CharacterId = 5 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 6, CharacterId = 6 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 7, CharacterId = 7 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 8, CharacterId = 8 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 9, CharacterId = 9 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 10, CharacterId = 10 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 11, CharacterId = 11 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 12, CharacterId = 12 });
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 2, ActorId = 13, CharacterId = 13 });

            // Iron Man 1
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 3, ActorId = 1, CharacterId = 1 }); // Iron Man
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 3, ActorId = 5, CharacterId = 5 }); // Black Widow
            // Iron Man 2
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 4, ActorId = 1, CharacterId = 1 }); // Iron Man
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 4, ActorId = 5, CharacterId = 5 }); // Black Widow
            // Iron Man 3
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 5, ActorId = 1, CharacterId = 1 });

            // Captain America: The First Avenger
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 6, ActorId = 2, CharacterId = 2 }); // Captain America
            // Captain America: The Winter Soldier
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 7, ActorId = 2, CharacterId = 2 }); // Captain America
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 7, ActorId = 5, CharacterId = 5 }); // Black Widow
            // Captain Ameria: Civil War
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 8, ActorId = 1, CharacterId = 1 }); // Iron Man
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 8, ActorId = 2, CharacterId = 2 }); // Captain America
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 8, ActorId = 5, CharacterId = 5 }); // Black Widow
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 8, ActorId = 12, CharacterId = 12 }); // Spider-Man
            builder.Entity<MovieCharacter>().HasData(new MovieCharacter { MovieId = 8, ActorId = 8, CharacterId = 8 }); // Ant-Man
        }
    }
}
