﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Context;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repositories;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly FranchiseRepository _repository;
        private readonly IMapper _mapper;

        public FranchisesController(FranchiseRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchises()
        {
            IEnumerable<Franchise> franchises = await _repository.GetAllAsync();
            IEnumerable<FranchiseDTO> franchiseDTOs = _mapper.Map<IEnumerable<FranchiseDTO>>(franchises);

            return Ok(franchiseDTOs);
        }

        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _repository.GetByIdAsync(id);

            if (franchise == null)
                return NotFound();

            FranchiseDTO franchiseDTO = _mapper.Map<FranchiseDTO>(franchise);

            return franchiseDTO;
        }

        // GET: api/Franchises/5/Complete
        [HttpGet("{id}/Complete")]
        public async Task<ActionResult<FranchiseFullDTO>> GetFranchiseDetailed(int id)
        {
            Franchise franchise = await _repository.Queryable
                .Where(f => f.Id.Equals(id))
                // Retrieve Actors
                .Include(f => f.HasMovies)
                .ThenInclude(m => m.MovieCharacters)
                .ThenInclude(a => a.Actor)
                // Retrieve Characters
                .Include(f => f.HasMovies)
                .ThenInclude(m => m.MovieCharacters)
                .ThenInclude(c => c.Character)
                // Retrieve Movies
                .Include(f => f.HasMovies)
                .SingleOrDefaultAsync();

            if (franchise == null)
                return NotFound();

            FranchiseFullDTO franchiseDTO = _mapper.Map<FranchiseFullDTO>(franchise);

            return franchiseDTO;
        }

        // GET: api/Franchises/5/Actors
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<FranchiseCharactersDTO>> GetFranchiseCharacters(int id)
        {
            Franchise franchise = await _repository.Queryable
                .Where(f => f.Id.Equals(id))
                .Include(f => f.HasMovies)
                .ThenInclude(m => m.MovieCharacters)
                .ThenInclude(a => a.Character)
                .SingleOrDefaultAsync();

            if (franchise == null)
                return NotFound();

            FranchiseCharactersDTO franchiseDTO = _mapper.Map<FranchiseCharactersDTO>(franchise);

            return franchiseDTO;
        }

        // GET: api/Franchises/5/Actors
        [HttpGet("{id}/Actors")]
        public async Task<ActionResult<FranchiseActorsDTO>> GetFranchiseActors(int id)
        {
            Franchise franchise = await _repository.Queryable
                .Where(f => f.Id.Equals(id))
                .Include(f => f.HasMovies)
                .ThenInclude(m => m.MovieCharacters)
                .ThenInclude(a => a.Actor)
                .SingleOrDefaultAsync();

            if (franchise == null)
                return NotFound();

            FranchiseActorsDTO franchiseDTO = _mapper.Map<FranchiseActorsDTO>(franchise);

            return franchiseDTO;
        }

        // GET: api/Franchises/5/Movies
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<FranchiseMoviesDTO>> GetFranchiseMovies(int id)
        {
            //Franchise franchise = await _repository.GetByIdAsync(id);
            Franchise franchise = await _repository.Queryable
                .Include(mc => mc.HasMovies)
                .Where(f => f.Id.Equals(id))
                .SingleOrDefaultAsync();

            if (franchise == null)
                return NotFound();

            FranchiseMoviesDTO franchiseDTO = _mapper.Map<FranchiseMoviesDTO>(franchise);

            return franchiseDTO;
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _repository.Update(franchise);

            try
            {
                await _repository.SaveAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            await _repository.InsertAsync(franchise);
            await _repository.SaveAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
        {
            Franchise franchise = await _repository.GetByIdAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _repository.Delete(franchise);
            await _repository.SaveAsync();

            return franchise;
        }

        private bool FranchiseExists(int id)
        {
            return _repository.Exists(id);
        }
    }
}
