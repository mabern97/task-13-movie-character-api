﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Context;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repositories;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly CharacterRepository _repository;
        private readonly IMapper _mapper;

        public CharactersController(CharacterRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharacters()
        {
            IEnumerable<Character> characters = await _repository.GetAllAsync();
            IEnumerable<CharacterShortDTO> characterDTOs = _mapper.Map<IEnumerable<CharacterShortDTO>>(characters);
            return Ok(characterDTOs);
        }

        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterShortDTO>> GetCharacter(int id)
        {
            Character character = await _repository.GetByIdAsync(id);

            if (character == null)
                return NotFound();

            CharacterShortDTO characterDTO = _mapper.Map<CharacterShortDTO>(character);

            return characterDTO;
        }

        /* GET: api/Characters/5/Movie
        [HttpGet("{id}/[action]")]
        public async Task<ActionResult<IEnumerable<Movie>>> GetCharacterMovies()
        {
            //var movies = await 
        }*/

        // PUT: api/Characters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _repository.Update(character);

            try
            {
                await _repository.SaveAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(Character character)
        {
            await _repository.InsertAsync(character);
            await _repository.SaveAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Character>> DeleteCharacter(int id)
        {
            Character character = await _repository.GetByIdAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _repository.Delete(character);
            await _repository.SaveAsync();

            return character;
        }

        private bool CharacterExists(int id)
        {
            return _repository.Exists(id);
        }
    }
}
