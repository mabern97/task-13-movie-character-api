﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Context;
using MovieCharacterAPI.Repositories;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.DTOs;
using AutoMapper;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        //private readonly ApplicationDbContext _context;
        private readonly ActorRepository _repository;
        private readonly IMapper _mapper;

        public ActorsController(ActorRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET: api/Actors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorShortDTO>>> GetActors()
        {
            IEnumerable<Actor> actors = await _repository.GetAllAsync();
            IEnumerable <ActorShortDTO> actorDtos = _mapper.Map<IEnumerable<ActorShortDTO>>(actors);

            return Ok(actorDtos);
        }

        // GET: api/Actors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorShortDTO>> GetActor(int id)
        {
            Actor actor = await _repository.GetByIdAsync(id);

            if (actor == null)
                return NotFound();

            ActorShortDTO actorDTO = _mapper.Map<ActorShortDTO>(actor);

            return actorDTO;
        }

        // GET: api/Actors/5/Details
        [HttpGet("{id}/All")]
        public async Task<ActionResult<ActorFullDTO>> GetActorProfile(int id)
        {
            //Actor actor = await _repository.GetByIdAsync(id);
            Actor actor = await _repository.Queryable
                // Include Movies
                .Include(mc => mc.InMovies)
                .ThenInclude(mc => mc.Movie)
                // Include Characters
                .Include(mc => mc.InMovies)
                .ThenInclude(mc => mc.Character)
                .Where(a => a.Id.Equals(id))
                .SingleOrDefaultAsync();

            if (actor == null)
                return NotFound();

            ActorFullDTO actorDTO = _mapper.Map<ActorFullDTO>(actor);

            return actorDTO;
        }

        // GET: api/Actors/5/Details
        [HttpGet("{id}/Details")]
        public async Task<ActionResult<ActorDTO>> GetActorDetails(int id)
        {
            Actor actor = await _repository.GetByIdAsync(id);

            if (actor == null)
                return NotFound();

            ActorDTO actorDTO = _mapper.Map<ActorDTO>(actor);

            return actorDTO;
        }

        // GET: api/Actors/5/Charactors
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<ActorCharacterDTO>> GetActorCharacters(int id)
        {
            Actor actor = await _repository.Queryable
                .Include(mc => mc.InMovies)
                .ThenInclude(mc => mc.Character)
                .Where(a => a.Id.Equals(id))
                .SingleOrDefaultAsync();

            if (actor == null)
                return NotFound();

            ActorCharacterDTO actorDTO = _mapper.Map<ActorCharacterDTO>(actor);

            return actorDTO;
        }

        // GET: api/Actors/5/Movies
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<ActorMovieDTO>> GetActorMovies(int id)
        {
            Actor actor = await _repository.Queryable
                .Include(mc => mc.InMovies)
                .ThenInclude(mc => mc.Movie)
                .Where(a => a.Id.Equals(id))
                .SingleOrDefaultAsync();

            if (actor == null)
                return NotFound();

            ActorMovieDTO actorDTO = _mapper.Map<ActorMovieDTO>(actor);

            return actorDTO;
        }

        // PUT: api/Actors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            //_context.Entry(actor).State = EntityState.Modified;
            _repository.Update(actor);

            try
            {
                //await _context.SaveChangesAsync();
                await _repository.SaveAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            //_context.Actors.Add(actor);
            //await _context.SaveChangesAsync();
            await _repository.InsertAsync(actor);

            return CreatedAtAction("GetActor", new { id = actor.Id }, actor);
        }

        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            Actor actor = await _repository.GetByIdAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _repository.Delete(actor);
            await _repository.SaveAsync();

            return actor;
        }

        private bool ActorExists(int id)
        {
            return _repository.Exists(id);
        }
    }
}
