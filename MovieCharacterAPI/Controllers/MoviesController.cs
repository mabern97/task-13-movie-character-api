﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Context;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repositories;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieRepository _repository;
        private readonly IMapper _mapper;

        public MoviesController(MovieRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieShortDTO>>> GetMovies()
        {
            IEnumerable<Movie> movies = await _repository.GetAllAsync();
            IEnumerable<MovieShortDTO> moviesDTO = _mapper.Map<IEnumerable<MovieShortDTO>>(movies);

            return Ok(moviesDTO);
        }

        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            Movie movie = await _repository.GetByIdAsync(id);

            if (movie == null)
                return NotFound();

            MovieShortDTO movieDTO = _mapper.Map<MovieShortDTO>(movie);

            return Ok(movieDTO);
        }

        // GET: api/Movies/5/Actors
        [HttpGet("{id}/Actors")]
        public async Task<ActionResult<MovieActorDTO>> GetMovieActors(int id)
        {
            Movie movie = await _repository.Queryable
                .Include(mc => mc.MovieCharacters)
                .ThenInclude(mc => mc.Actor)
                .Where(m => m.Id.Equals(id))
                .SingleAsync();

            if (movie == null)
                return NotFound();

            MovieActorDTO movieDTO = _mapper.Map<MovieActorDTO>(movie);

            return movieDTO;
        }

        // GET: api/Movies/5/Characters
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<MovieCharacterDTO>> GetMovieCharacters(int id)
        {
            Movie movie = await _repository.Queryable
                .Include(mc => mc.MovieCharacters)
                .ThenInclude(mc => mc.Character)
                .Where(m => m.Id.Equals(id))
                .SingleAsync();

            if (movie == null)
                return NotFound();

            MovieCharacterDTO movieDTO = _mapper.Map<MovieCharacterDTO>(movie);

            return movieDTO;
        }

        // PUT: api/Movies/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _repository.Update(movie);

            try
            {
                await _repository.SaveAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(Movie movie)
        {
            await _repository.InsertAsync(movie);
            await _repository.SaveAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Movie>> DeleteMovie(int id)
        {
            Movie movie = await _repository.GetByIdAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _repository.Delete(movie);
            await _repository.SaveAsync();

            return movie;
        }

        private bool MovieExists(int id)
        {
            return _repository.Exists(id);
        }
    }
}
